/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import configStore from './state/configStore';
import { registerScreens } from './state/screens';
import { studentViewContainer, animationViewContainer } from './views/containers';

export default class App {
  static startApp() {

    registerScreens(configStore(), Provider);

    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setRoot({
        root: {
          bottomTabs: {
            children: [{
              stack: {
                children: [{
                  component: {
                    name: studentViewContainer.displayName,
                    passProps: {
                      text: 'some props may need to pass'
                    }
                  }
                }],
                options: {
                  bottomTab: {
                    text: 'GRADES',
                    icon: require('./common/images/Rates.png'),
                    selectedIcon: require('./common/images/Rates-Active.png'),
                    testID: studentViewContainer.displayName
                  }
                }
              }
            },
            {
              component: {
                name: animationViewContainer.displayName,
                passProps: {
                  animating: true
                },
                options: {
                  bottomTab: {
                    text: 'ANIMATIONS',
                    icon: require('./common/images/Updates.png'),
                    selectedIcon: require('./common/images/Updates-Active.png'),
                    testID: animationViewContainer.displayName
                  }
                }
              }
            },
            ]
          }
        }
      });
    });
  }
}

