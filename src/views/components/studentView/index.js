import React, {
  PureComponent
} from 'react';

import {
  View,
  Text,
  StatusBar,
  FlatList
} from 'react-native';

import PropTypes from 'prop-types';

import Styles from './styles';
import ContentCell from './contentCell';
//React.PropTypes = PropTypes;
export default class StudentView extends PureComponent {


  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.someStudentRequest();
  }

  onClick = (e) => {
    console.log(e);
  }

  render() {
    const { studentReducer: { gradesSubReducer: { studentData } } } = this.props;
    return (
      <View>
        <StatusBar barStyle={'dark-content'} />
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <FlatList
            data={studentData}
            renderItem={({ item }) => <ContentCell data={item} action={this.onClick} />}
            keyExtractor={item => item.name} // NOTE: it should be an unique ID instead of name or other attributes, using name here just for avoiding warning yellow box
          />
        </View>
      </View>
    );
  }
}
