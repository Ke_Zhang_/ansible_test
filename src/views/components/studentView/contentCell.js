import React from 'react';
import {
    View,
    Text,
    TouchableHighlight
} from 'react-native';
import Styles from './styles';

const ContentCell = ({ data,action }) => {

    return (
        <TouchableHighlight onPress={()=>action('something is pressed!')} underlayColor='#edeeed'>
              <View style={Styles.cellContainer}>
                  <View style={Styles.content}>
                      <Text style={Styles.nameText}>
                          {data.name}
                      </Text>
                      <Text style={Styles.scoreText}>
                          {data.grade}
                      </Text>
                  </View>
              </View>
        </TouchableHighlight>
    )
};

export default ContentCell