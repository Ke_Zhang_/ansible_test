import { StyleSheet } from 'react-native';
import CommonStyle from '../../../common/commonStyles';

// const { SCREEN, THEME_COLOR, fonts } = COMMON_STYLE;
// const threeImgWidth = (SCREEN.width - 52) / 3;
// const twoImgWidth = (SCREEN.width - 40) / 2;
const Styles = StyleSheet.create({
    title: {
        ...CommonStyle.fonts.M2,
    },
    cellContainer: {
        width:CommonStyle.screen.width * 0.9,
        height: 40,
        backgroundColor:CommonStyle.themeColor.color,
        borderRadius:7,
        shadowOpacity: 0.4,
        shadowOffset: {
            width: 0, height: 2
        },
        shadowColor: CommonStyle.themeColor.black,
        elevation: 4,
        marginTop:6
    },
    nameText: {
        ...CommonStyle.fonts.XL,
        color: CommonStyle.themeColor.whiteSmoke,
    },
    scoreText: {
        ...CommonStyle.fonts.XL,
        color: CommonStyle.themeColor.whiteSmoke,
    },
    content: {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        height:40,
        paddingHorizontal:20
    }
});

export default Styles;