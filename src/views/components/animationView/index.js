import React, {
  PureComponent
} from 'react';

import {
  View,
  Text,
  StatusBar,
  ART,
  TouchableOpacity
} from 'react-native';


import CircleSnail from './CircleSnail';

//import PropTypes from 'prop-types';

import Styles from './styles';
import { Animated } from 'react-native';
//React.PropTypes = PropTypes;

export default class AnimationView extends PureComponent {


  constructor(props) {
    super(props);
    this.state = {
      progress: 0,
      indeterminate: false,
      animating: true,
      size: 300
    };
  }

  onClick = (e, a) => {
    this.setState({
      animating: e,
    })
  }

  // componentDidMount(){
  //   const initialCircles = this.props.animationSubReducer.createCircles();
  //   console.log(this.props);
  // }

  render() {
    return (
      <View>
        <StatusBar barStyle={'dark-content'} />
        <View style={Styles.animationContainer}>
          <CircleSnail
            circles = {this.props.circles}
            style={{ margin: 0}}
            animating={this.state.animating}
          />
        </View>
      </View>
    );
  }
}
