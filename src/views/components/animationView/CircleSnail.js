import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated,Easing, View } from 'react-native';
import ArrayCircle from './arrayCircle';



const MIN_ARC_ANGLE = 0;
const MAX_ARC_ANGLE = 2 * Math.PI;

export default class CircleSnail extends Component {
    static propTypes = {
        animating: PropTypes.bool,
    };

    static defaultProps = {
        animating: true,
    };

    constructor(props) {
        super(props);
    }


    componentDidMount() {
        if (this.props.animating) {
            this.animate();
        }
    }

    animate() {
        let arrayAnimations = [];
        this.props.circles.map((v, i) => {
            arrayAnimations[i] = Animated.timing(v.startAngle, {
                toValue: -MAX_ARC_ANGLE - MIN_ARC_ANGLE,
                duration: v.duration || 1000,
                isInteraction: false,
                easing: Easing.inOut(Easing.quad)
            })
        })
        Animated.stagger(500, arrayAnimations).start();
    }

    render() {
        const {
            animating,
            style,
            circles,
            ...restProps
    } = this.props;

        //const { size } = this.state;

        if (!animating) {
            return null;
        }
        return (
            <Animated.View
                {...restProps}
                style={[
                    style,
                    {
                        backgroundColor: 'transparent',
                    },
                ]}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    {
                        circles.map((value, i) => {
                            return (
                                <ArrayCircle
                                    direction={'clockwise'}
                                    size={value.size}
                                    radius={value.radius}
                                    offset={value.offset}
                                    startAngle={value.startAngle}
                                    endAngle={value.endAngle}
                                    strokeCap={value.strokeCap}
                                    thickness={value.thickness}
                                    //color={['#F44336', '#2196F3', '#009688']}
                                    //colorIndex={this.state.colorIndex}
                                    color={value.color}
                                    key={i}
                                />
                            )
                        })
                    }
                </View>
            </Animated.View>
        )
    }
}
