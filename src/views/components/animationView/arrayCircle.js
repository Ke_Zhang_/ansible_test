import React from 'react';
import Styles from './styles';
import { Animated, ART, Easing, View, Text } from 'react-native';

import Arc from './Arc';

const AnimatedArc = Animated.createAnimatedComponent(Arc);

const ArrayCircle = ({ size, radius,offset,startAngle,endAngle,strokeCap,thickness,color,direction,colorIndex }) => {

    return (
            <View style={{ position: 'absolute' }}>
                <ART.Surface width={size} height={size}>
                    <AnimatedArc
                        direction={
                            direction === 'counter-clockwise'
                                ? 'clockwise'
                                : 'counter-clockwise'
                        }
                        radius={radius}
                        stroke={color}
                        //stroke={Array.isArray(color) ? color[colorIndex] : color}
                        offset={offset}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        strokeCap={strokeCap}
                        strokeWidth={thickness}
                    />
                </ART.Surface>
            </View>
    )
};

export default ArrayCircle