import React from 'react';
import { connect } from 'react-redux';
import { connectEnhance } from '../../state/screens';
import { bindActionCreators } from 'redux';
import { animationActions } from '../../state/ducks';
//import * as viewActions from '../actions/updatesViewActions.js';
import { Animated } from 'react-native';

const MIN_ARC_ANGLE = 0;
const MAX_ARC_ANGLE = 2 * Math.PI;

import AnimationView from '../components/animationView';


const animationViewContainer = ({ ...props }) => {
    // do some business logic here or in reducer as initial state
    let circles = [];
    let circlesGreen = [];
    let circlesRed = [];
    for (let i = 300; i >= 1; i -= 10) {
        circlesGreen.push({
            size: i,
            radius: i / 2 - 3,
            offset: {
                top: 3,
                left: 3,
            },
            startAngle: new Animated.Value(0),
            endAngle: new Animated.Value(0),
            strokeCap: 'round',
            thickness: 2,
            color: '#8dd052',
            duration: 500
        })
    }

    for (let i = 295; i >= 1; i -= 10) {
        circlesRed.push({
            size: i,
            radius: i / 2 - 3,
            offset: {
                top: 3,
                left: 3,
            },
            startAngle: new Animated.Value(0),
            endAngle: new Animated.Value(0),
            strokeCap: 'round',
            thickness: 2,
            color: '#e52163',
            duration: 500
        })
    }

    circles = [...circlesGreen,...circlesRed];

    

    return (
        <AnimationView {...props} circles={circles} />
    )
};

function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...animationActions }, dispatch);
}


export default connectEnhance(animationViewContainer, mapStateToProps, mapDispatchToProps);