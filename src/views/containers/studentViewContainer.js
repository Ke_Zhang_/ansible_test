import React from 'react';
import { connect } from 'react-redux';
import { connectEnhance } from '../../state/screens';
import { bindActionCreators } from 'redux';
import { studentActions } from '../../state/ducks';
//import * as viewActions from '../actions/updatesViewActions.js';

import StudentView from '../components/studentView';

const studentViewContainer = ({ ...props }) => (
    <StudentView {...props} />
);

function mapStateToProps(state) {
  return {
    studentReducer: state.studentsReducers
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...studentActions}, dispatch);
}


export default connectEnhance(studentViewContainer, mapStateToProps, mapDispatchToProps);