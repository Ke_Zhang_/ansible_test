import { delay } from 'redux-saga';
import {
    fork,
    cancel,
    call,
    put,
    race,
    take,
    takeLatest,
    END
} from 'redux-saga/effects';
import axios from "axios";
import actionTypes from './types';
//import * as updatesViewActions from '../actions/updatesViewActions';

function* animationRequest() {
    while (true) {
        const animationRequest = yield take(actionTypes.GET_ANIMATIONS);
        const data = yield call(fetchAnimation);
        yield put({type: actionTypes.GET_ANIMATIONS_RESPONSE,payload:data.data});
    }
}

function fetchAnimation() { // this is a real api call
    return {} // something else will be returned as promise
}

export default function* animationSaga() {
    yield [
        fork(animationRequest),
    ];
}