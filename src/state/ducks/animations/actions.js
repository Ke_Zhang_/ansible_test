import types from "./types";

const someAnimationRequest = request => ({
  type: types.GET_ANIMATIONS,
  request
});

const someAnimationResponse = response => ({
  type: types.GET_ANIMATIONS_RESPONSE,
  response
});


export default {
    someAnimationRequest,
    someAnimationResponse
};