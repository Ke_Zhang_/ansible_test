import { combineReducers } from "redux";
import { Animated } from 'react-native';


const animationSubReducer = (state = {

}, action) => {
  switch (action.type) {
    default:
      return state;
  }
}

const animationReducers = combineReducers({
  animationSubReducer
});

export default animationReducers;