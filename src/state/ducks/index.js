import { combineReducers } from "redux";
import { fork } from 'redux-saga/effects';


import * as animationsState from "./animations";
import * as studentsState from "./studentGrades"


const animationsReducers = animationsState.animationReducers;
const studentsReducers = studentsState.studentsReducers;
const studentActions = studentsState.studentActions;
const animationActions = animationsState.animationActions;
const rootReducer = combineReducers({
    animationsReducers,
    studentsReducers
});

export {
  rootReducer,
  studentActions,
  animationActions
}

export default function* rootSaga() {
  yield [
    fork(animationsState.animationSaga),
    fork(studentsState.studentsSaga),
  ];
}


