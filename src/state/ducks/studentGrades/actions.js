import types from "./types";

const someStudentRequest = request => ({
  type: types.GET_GRADES,
  request
});

const someStudentResponse = response => ({
  type: types.GET_GRADES_RESPONSE,
  response
});


export default {
    someStudentRequest,
    someStudentResponse
};