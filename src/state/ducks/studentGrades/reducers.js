import { combineReducers } from "redux";
import actionTypes from "./types";

const gradesSubReducer = (state = {
}, action) => {
  switch (action.type) {
    case actionTypes.GET_GRADES_RESPONSE:
      // do some business logic to the data either here or in containers, but NOT in representational components.
      let sortedGrades = action.payload;
        sortedGrades.sort(
          (a,b) => {
            return b.grade - a.grade
          }
        )
      return {
        studentData: sortedGrades
      }
    default:
      return state;
  }
}

// some other sub-reducers go here

const studentsReducers = combineReducers({
  gradesSubReducer
});

export default studentsReducers;