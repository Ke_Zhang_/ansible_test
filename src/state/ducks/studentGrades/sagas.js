import { delay } from 'redux-saga';
import {
    fork,
    cancel,
    call,
    put,
    race,
    take,
    takeLatest,
    END
} from 'redux-saga/effects';
import axios from "axios";
import actionTypes from './types';



function* studentRequest() {
    while (true) {
        const studentRequest = yield take(actionTypes.GET_GRADES);
        const data = yield call(fetchGrades);
        yield put({type: actionTypes.GET_GRADES_RESPONSE,payload:data.data});
    }
}

function fetchGrades() { // this is a real api call, it's better to organize all api calls in another file
    return axios.get("http://filestore.mnetgroup.com/output.html");
}

export default function* studentsSaga() {
    yield [
        fork(studentRequest),
    ];
}