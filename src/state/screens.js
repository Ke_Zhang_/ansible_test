import { connect } from 'react-redux';
import { compose } from 'redux';
import { Navigation } from 'react-native-navigation';
import withSafeAreaView from '../views/components/hocs/withSafeAreaView';

let screens = [];

export function connectEnhance(Component, mapStateToProps = function () { return {} }, mapDispatchToProps = function () { return {} }, mergeProps, options) {
  const connected = connect(mapStateToProps, mapDispatchToProps, mergeProps, options);
  const enhance = compose(
    connected
  )
  let Screen = enhance(Component);
  Screen.displayName = Component.name+screens.length; // this is the main reason to enhance connect, in previous version of react-native-navigation, without adding a number after screen display name will results error displaying when run on production devices.
  
  screens.push(Screen);
  return Screen;
}

export function registerScreens(store, Provider) {
  screens.forEach((Screen) => {
    Navigation.registerComponentWithRedux(
      Screen.displayName,
      //() => Screen,
      () => withSafeAreaView(Screen), // wrap all components with safe area on iPhoneX,XR and X MAX
      Provider,
      store
    );
  });
}