/*
Problem: Given that I need to download x images in a row, which are named: img1, img2, etc., write a function which can download these images.
1. The url format is  exampleserver.com/img1  etc
2. The code should have a call back.
3. Should wait for each image to finish before starting the next download.
4. Should confirm that all the images have downloaded.
5. Should store the images in the app user location.
6. The server will provide a response header with number of images to
fetch, e.g. response header: “Images”: 10
7. The code should be neat.
8. Should use helper functions to make the code well organized.
*/

const urlpr = "http://exampleserver.com/img";
let allCount = 10;

function save(res) {
    return new Promise((resolve,reject)=> {
        // doing some logic to save to mobile phone local path
        if(res.save){
            resolve()
        }else{
            reject()
        }
    })
}

function downloadImage(count) {
    let url = urlpr + count;
    return new Promise((resolve, reject) => {
        RNFetchBlob.config({ fileCache: true }).fetch('GET', url).then(res => {
            return save(res)
        }).then(() => {
            let c = count++;
            if (c < allCount) {
                return downloadImage(count++)
            } else {
                resolve()
            }
        }).catch(error => {
            reject(error)
        })

    })
}
